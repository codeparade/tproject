<?php

require_once('config.php');
require_once('DatabaseConnection.php');

class UserManager
{
    //Unused since mysql procedure
    public static function addUser($userName, $showName, $firstName, $lastName, $password, $userType, $email)
    {
        $conn = new DatabaseConnection();

		$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

		try
		{
            $result = $conn->getHandle()->prepare("INSERT INTO users VALUES(NULL,?,?,?,?,?,?,?,?, NOW())");
            $r = $result->execute([$userName, $showName, $firstName, $lastName, password_hash($password, PASSWORD_BCRYPT), $email, false , $userType]);    
            
            if(!$result)
            {
                return false;
            }

            $result = $conn->getHandle()->prepare("SELECT userId FROM users WHERE userName = ?");
            $r = $result->execute([$userName]);    
                   
            //Check if OKAY
            if($result->rowCount() != 1)
            {
                return false;
            }
            else
            {
                return $result->fetch();
            }
		}
		catch(PDOException $e)
		{
			throw new Exception("Failed to query DB");
		}

        $conn->disconnect();
    } 

    //Remove user
    public static function removeUser($userId)
    {
        $conn = new DatabaseConnection();

		$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

		try
		{
            $result = $conn->getHandle()->prepare("DELETE FROM users WHERE userId = ?");
            $r = $result->execute([$userId]);            
		}
		catch(PDOException $e)
		{
			throw new Exception("Failed to query DB");
		}

        $conn->disconnect();
    }

    //Validate user on logon
    public static function validateUser($userName, $password)
    {
        $conn = new DatabaseConnection();

		$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

		try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM users WHERE username = ?");
            $r = $result->execute([$userName]);
            
            if($result->rowCount() != 1)
            {
                return false;
            }
            else
            {
                $res =  $result->fetch();

                if(password_verify($password, $res['password']))
                {
                    //Clear array before return
                    foreach ($res as $key => $value)
                    {
                        if (is_int($key))
                        {
                            unset($res[$key]);
                        }
                    }

                    //Remove pwd
                    unset($res["password"]);

                    return $res;
                }
                else
                {
                    return false;
                }
              
            }
		}
		catch(PDOException $e)
		{
			throw new Exception("Failed to query DB");
		}

        $conn->disconnect();
    }


    //Change password
    public static function changeUserPasswd($uid, $newPwd, $oldPwd)
    {

        //Database
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
        {
            $result = $conn->getHandle()->prepare("SELECT password FROM users WHERE userId = ?");
            $r = $result->execute([$uid]);

            if($result->rowCount() == 1)
            {
                $res = $result->fetchAll();
                if(password_verify($oldPwd, $res[0]['password']))
                {
                  $result = $conn->getHandle()->prepare("UPDATE users SET password=? WHERE userId=?");
                  $r = $result->execute([password_hash($newPwd, PASSWORD_BCRYPT), $uid]);
                  return true;
                }
                else
                {
                    throw new Exception("Invalid old password");
                    return false;
                }
            }
            else
            {
                throw new Exception("Invalid user");
                return false;
            }
        }
        catch(PDOException $e)
        {
             throw new Exception("Failed to query DB");
        }

        $conn->disconnect();
    }
}

?>