<?php

class SessionInfo
{
	public $sessionId;
	public $sessionOwner;
	public $serverIp;
	public $ownerIp;
	public $userShowName;
	public $sessionToken;
	public $sessionValidity;
	public $sessionUA;

	//Return as JSON
	public function toJSON()
	{
		return json_encode($this);
	}

	//Convert from JSON
	public static function fromJSON($json)
	{
		$js = json_decode($json, true);

		$toRet = new SessionInfo();
		foreach ($js as $key => $value) {
			$toRet->{$key} = $value;
		}

		return $toRet;
	}
}

?>