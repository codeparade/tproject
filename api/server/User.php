<?php

//We need to Implement JSONSerializable iface due to problem with JSON_ENCODE
class User  implements JsonSerializable
{
    //Gets User from ID
    public static function getFromId($uid)
    {
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM users WHERE userId=?");
            $r = $result->execute([$uid]);     
            
            $r = $result->fetchAll();

            $obj = new User();
            $obj->userId = $uid;
            $obj->userName = $r[0]['username'];
            $obj->userShowname = $r[0]['showname'];
            $obj->userFirstName = $r[0]['firstName'];
            $obj->userLastName = $r[0]['lastName'];
            $obj->userEmail = $r[0]['email'];
            $obj->userWasEverPremium = $r[0]['wasEverPremium'];
            $obj->userTypeId = $r[0]['userTypeId'];
            $obj->userRegData = $r[0]['registrationDate'];
            
            return $obj;
		}
		catch(PDOException $e)
		{
            
        }
        
    }

    //Gets UserID
    public function getUserId()
    {
        return $this->userId;
    }

    //Sets User ID
    public function setUserId($id)
    {
        $this->userId = $id;
    }

    //Get Username
    public function getUsername()
    {
        return $this->userName;
    }

    //Set Username
    public function setUsername($uname)
    {
        $this->userName = $uname;
    }

    //Get Showname
    public function getShowname()
    {
        return $this->userShowname;
    }

    //Set Showname
    public function setShowname($sname) 
    {
        $this->userShowName = $sname;
    }

    //Get Firstname
    public function getFirstname()
    {
        return $this->userFirstName;
    }

    //Set Firstname
    public function setFirstname($fname) 
    {
        $this->userFirstName = $fname;
    }

    //Get Lastname
    public function getLastname()
    {
        return $this->userLastName;
    }

    //set Lastname
    public function setLastname($lname) 
    {
        $this->userLastName = $lname;
    }

    //Get Email
    public function getEmail()
    {
        return $this->userEmail;
    }

    //Set Email
    public function setEmail($mail) 
    {
        $this->userEmail = $mail;
    }

    //Was User Ever Premium?
    public function getWasUserEverPremium()
    {
        return $this->userWasEverPremium;
    }

    //Set Premium status
    public function setWasUserEverPremium($premium) 
    {
        $this->userWasEverPremium = $premium;
    }

    //Get UserType ID
    public function getUserTypeId()
    {
        return $this->userTypeId;
    }

    //Set UserType ID
    public function setUserTypeId($uti) 
    {
        $this->userTypeId = $uti;
    }

    //Get Register Date
    public function getRegDate()
    {
        return $this->userRegData;
    }

    //Set Register Date
    public function setRegDate($regD) 
    {
        $this->userRegData = $regD;
    }

    //JSON Serialize
    public function jsonSerialize(){
        return [
            "userId" => $this->userId,
            "userName"  => $this->userName,
            "userShowname" => $this->userShowname,
            "userFirstName" => $this->userFirstName,
            "userLastName" => $this->userLastName,
            "userEmail" => $this->userEmail,
            "userWasEverPremium" => $this->userWasEverPremium,
            "userTypeId" => $this->userTypeId,
            "userRegData" => $this->userRegData
        ];
    }

    //Members
    protected $userId;
    protected $userName;
    protected $userShowname;
    protected $userFirstName;
    protected $userLastName;
    protected $userEmail;
    protected $userWasEverPremium;
    protected $userTypeId;
    protected $userRegData;
}

?>