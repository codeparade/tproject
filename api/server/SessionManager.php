<?php

require_once('SessionInfo.php');
require_once('DatabaseConnection.php');

class SessionManager
{
	//Register Session
	public static function registerNewSession($userId, $userShowName)
	{
		$sess = new SessionInfo();

		$sess->sessionId = SessionManager::generateId();
		$sess->sessionOwner = $userId;
		$sess->serverIp = $_SERVER['SERVER_NAME'];
		$sess->ownerIp = $_SERVER['REMOTE_ADDR'];
		$sess->userShowName = $userShowName;
		$sess->sessionToken = SessionManager::generateToken($sess->sessionId);
		$sess->sessionValidity = time() + 1800;
		$sess->sessionUA = $_SERVER['HTTP_USER_AGENT'];

		//Store into database
		$conn = new DatabaseConnection();

		$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

		try
		{
			$conn->getHandle()->prepare("INSERT INTO sessions VALUES (NULL, ?, ? ,FROM_UNIXTIME(?), ?)")->execute([$userId , $sess->sessionId , $sess->sessionValidity, base64_encode($sess->toJSON())]);
		}
		catch(PDOException $e)
		{
			echo "err";
		}

		$conn->disconnect();

		return $sess;
	}

	//Destory Session
	public static function destroySession($sessionId)
	{
		//Remove session from Db
		$conn = new DatabaseConnection();

		$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

		try
		{
			$conn->getHandle()->prepare("DELETE FROM sessions WHERE sessionImplId=?")->execute([$sessionId]);
		}
		catch (PDOException $e)
		{
			echo "err";
		}

		$conn->disconnect();
		
	}

	//Validate Session
	public static function validateSession($sessionData, $renew = true, $retUid = false)
	{
		try
		{
			$sessionData = stripslashes($sessionData);

			$dt = SessionInfo::fromJSON($sessionData);

			//Hijacking prevention: NOT REQUIRED HERE
			// if($dt->serverIp != $_SERVER['SERVER_NAME'] || $dt->ownerIp != $_SERVER['REMOTE_ADDR'] || $dt->sessionUA != $_SERVER['HTTP_USER_AGENT'])
			// {
			// 	throw new Exception("Bad Signature");
			// }

			//Validate in database
			$conn = new DatabaseConnection();

			$conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);


			$result = $conn->getHandle()->query("SELECT sessionData FROM vActiveUsers")->fetchAll();

			$found = false;
	
			//Check if we have users in active users
			foreach ($result as $key => $value) {

				if(stripslashes(base64_decode($value[0])) === $sessionData)
				{
					$found = true;
					break;
				}
			}

			if(!$found)
			{
				throw new Exception("Session not found in database");
			}

			$conn->disconnect();

			if($dt->sessionValidity < time())
			{	
				return null;
			}
			else
			{
				if($renew)
				{
					$dt = SessionManager::renewSession($dt);
					return $dt;
				}
				else
				{
					if($retUid)
					{
						return $dt;
					}
					else
					{
						return true;
					}
				}
			}
		}
		catch (Exception $e)
		{
			throw new Exception("Invalid Session:".$e->getMessage());
		}

	}

	//Renew Session
	public static function renewSession($sessionData)
	{
		$ss = SessionManager::registerNewSession($sessionData->sessionOwner, $sessionData->userShowName);

		SessionManager::destroySession($sessionData->sessionId);

		return $ss;
	}

	//Gen sessid
	private static function generateId()
	{
		return hash("sha512", $_SERVER['REMOTE_ADDR'].random_bytes(3).$_SERVER['REMOTE_PORT'].random_bytes(6).time().random_bytes(10));
	}

	//Gen token
	private static function generateToken($sessionId)
	{
		return hash("sha512", $sessionId.random_bytes(8).crc32($sessionId));
	}
}

?>