<?php

require_once("User.php");

//Current User  - Singleton
class CurrentUser extends User
{
    //Singleton Pattern
    private static $instance;
    private function __construct() 
    {

    }

    private function __clone() 
    {

    }

    //Gets instance
    public static function getInstance()
    {
        if(self::$instance === null)
        {
            self::$instance = new CurrentUser();
        }

        return self::$instance;
    }

    //Parse Current User from Session
    public static function parseFromSession($sessData)
    {
        $sessionData = stripslashes($sessData);

        $dt = SessionInfo::fromJSON($sessionData);

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM users WHERE userId=?");
            $r = $result->execute([$dt->sessionOwner]);

            if($result->rowCount() != 1)
            {
                throw new Exception("No user");
            }
            else
            {
                //User Exists
                self::$instance = User::getFromId($dt->sessionOwner);
            }

		}
		catch(PDOException $e)
		{
            //Nothing
		}
    }
}

?>