<?php

require_once('DatabaseConnection.php');
require_once('CurrentUser.php');
require_once("Task.php");

//We need to Implement JSONSerializable iface due to problem with JSON_ENCODE
class Table implements JsonSerializable
{
    //Rets user tables as array of objects
    public static function getUserTables()
    {
        $uid = CurrentUser::getInstance()->getUserId();

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            //Call Proc
            $result = $conn->getHandle()->prepare("SELECT tabId FROM userTable WHERE userId=?");
            $r = $result->execute([$uid]);

            //Num rows and fetch tables
            $res = $result->fetchAll();

            $toRet = array();
            $i = 0;
            foreach ($res as $row) 
            {
                $toRet['a'.$row['tabId']] = Table::getTable($row['tabId'], $i);
                $i++;
            }

            return $toRet;        
		}
		catch(PDOException $e)
		{
            //Nothin to do here
		}
    }

    //Gets Table from Id
    public static function getTable($tableId, $idd = 0)
    {
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM tables WHERE tabId=?");
            $r = $result->execute([$tableId]);

            $res = $result->fetchAll();

            //Construct new Obj
            $obj = new Table();
            $obj->tableId = $res[0]['tabId'];
            $obj->title = $res[0]['title'];
            $obj->priority = $idd;
            $obj->owner = User::getFromId($res[0]['ownerId']);
            $obj->bgUrl = $res[0]['bgUrl'];
            $obj->fetchTasks();
            $obj->fetchUsers();
            
            return $obj;
		}
		catch(PDOException $e)
		{
            //Nothing to do here
		}
    }

    //Ctor.
    private function __construct() 
    {
        
    }

    //Gets Title
    public function getTitle()
    {
        return $this->title;
    }

    //Sets Title
    public function setTitle($newTitle)
    {
        $this->title = $newTitle;
    }

    //Gets Priority
    public function getPriority()
    {
        return $this->priority;
    }

    //Sets Priority
    public function setPriority($newPriority)
    {
        $this->priority = $newPriority;
    }

    //Gets Owner
    public function getOwner()
    {
        return $this->owner;
    }

    //Sets Owner
    public function setOwner($newOwner)
    {
        $this->owner = $newOwner;
    }

    //Gets Owner
    public function getBgUrl()
    {
        return $this->bgUrl;
    }

    //Sets Owner
    public function setBgUrl($bgUrl)
    {
        $this->bgUrl = $bgUrl;
    }

    //Serialize to JSON
    public function jsonSerialize()
    {
        return 
        [
            "tableId" => $this->tableId,
            "title"  => $this->title,
            //"id" => $this->priority,
            "id" => 'a'.$this->tableId,
            "owner" => json_encode($this->owner),
            "tasks" => json_encode($this->tasks),
            "users" => json_encode($this->users),
            "bgUrl" => $this->bgUrl
        ];
    }

    //Fetch Tasks on board
    public function fetchTasks()
    {
        
        $this->tasks = array();

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM tasks WHERE tableId=?");
            $r = $result->execute([$this->tableId]);     

            $res = $result->fetchAll();


            $i = 0;
            foreach ($res as $row) 
            {
//                array_push($this->tasks,Task::getTask($row['taskId'], $i));
                $this->tasks['a'.strval($row['taskId'])] = Task::getTask($row['taskId'], $i);
                $i++;
            }
		}
		catch(PDOException $e)
		{
            //Nothing
		}
    }

    //Fetch users with table access
    public function fetchUsers()
    {
        $this->users = array();

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM userTable WHERE tabId=?");

            $r = $result->execute([$this->tableId]);

            $res = $result->fetchAll();

            foreach ($res as $row) 
            {
                array_push($this->users, User::getFromId($row['userId']));
            }
		}
		catch(PDOException $e)
		{
            //Nothing
		}
    }

    //Add Task to Table
    public function addTask($task)
    {
        //Check if have we task
        if($this->hasTask($task))
        {
            return;
        }

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("INSERT INTO tasks VALUES(NULL,?,?,?,?,?,?,?)");
            $r = $result->execute([$task->getName(), $task->getDesc(), $task->getDeadline(), $task->getColor(), $task->getCreator()->getUserId(), $this->tableId, 0]); 
            
            echo $conn->getHandle()->errorInfo();
		}
		catch(PDOException $e)
		{
            //Nothing
        }
        
        array_push($this->tasks, $task);
    }

    //Add Perms for user to use this table 
    public function addUser($user)
    {
        //Check if have we task
        if($this->hasUserAccess($user))
        {
            return;
        }

        if($user->GetUserId() == $this->getOwner()->getUserId())
        {
            return;
        }

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("INSERT INTO userTable VALUES(?,?)");
            $r = $result->execute([$user->getUserId(), $this->tableId]);            
		}
		catch(PDOException $e)
		{
            //Nothing
        }
        
        array_push($this->users, $user);
    }

    //Has Board Task?
    public function hasTask($task)
    {
        foreach($this->tasks as $tsk)
        {
            if($tsk->getId() == $task->getId())
            {
                return true;
            }
        }

        return false;
    }

    //Is User Owner of a board?
    public function isUserOwner($user)
    {
        if($user->getUserId() == $this->owner->getUserId())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Has User Access to board?
    public function hasUserAccess($user)
    {
        foreach($this->users as $usr)
        {
            if($user->getUserId() == $usr->getUserId())
            {
                return true;
            }
        }

        return false;
    }

    //Remove Task from board
    public function removeTaskFromBoard($task)
    {
        //Check if have we task
        if(!$this->hasTask($task))
        {
            return;
        }

        //Perform on DB
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("DELETE FROM tasks WHERE taskId=?");
            $r = $result->execute([$task->getId()]);            
		}
		catch(PDOException $e)
		{
            //Nothing
        }
        
        array_push($this->users, $user);
    }

    //Return Task
    public function getTask($taskId)
    {
        foreach($this->tasks as $tsk)
        {
            if($tsk->getId() == $taskId)
            {
                return $tsk;
            }
        }
    }

    //Modify Task
    public function modifyTask($taskId, $action, $newData)
    {
        foreach($this->tasks as $tsk)
        {
            if($tsk->getId() == $taskId)
            {
                switch($action)
                {
                    case "NAME":
                    $tsk->setName($newData);
                    break;
                    case "DESC":
                    $tsk->setDesc($newData);
                    break;
                    case "DEADL":
                    $tsk->setDeadline($newData);
                    break;
                    case "COLOR":
                    $tsk->setColor($newData);
                    break;
                }

                 $task = $tsk;

                 $conn = new DatabaseConnection();

                 $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);
         
                 try
                 {
                     $result = $conn->getHandle()->prepare("UPDATE tasks SET name=?, description=?, deadline=?, color=?");
                     $r = $result->execute([$task->getName(), $task->getDesc(), $task->getDeadline(), $task->getColor()]); 
                     
                     echo $conn->getHandle()->errorInfo();
                 }
                 catch(PDOException $e)
                 {
                     //Nothing
                 }
                 
                 array_push($this->tasks, $task);
            }
        }
    }

    //Members
    private $tableId;
    private $title;
    private $priority;
    private $owner;
    private $bgUrl;
    private $tasks;
    private $users;

}

?>