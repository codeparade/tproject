<?php

require_once('DatabaseConnection.php');
require_once('CurrentUser.php');
require_once("Event.php");

//We need to Implement JSONSerializable iface due to problem with JSON_ENCODE
class Calendar implements JsonSerializable
{
        //Rets user calendars as array of objects
        public static function getUserCalendars()
        {
            $uid = CurrentUser::getInstance()->getUserId();

            $conn = new DatabaseConnection();

            $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

            try
            {
                //Call Proc
                $result = $conn->getHandle()->prepare("SELECT calendarId FROM userCalendar WHERE userId=?");
                $r = $result->execute([$uid]);

                //Num rows and fetch tables
                $res = $result->fetchAll();

                $toRet = array();
                foreach ($res as $row) 
                {
                    array_push($toRet, Calendar::getCalendar($row['calendarId']));
                }

                return $toRet;        
            }
            catch(PDOException $e)
            {
                //Nothin to do here
            }
        }

        //Gets Calendar from Id
        public static function getCalendar($calId)
        {
            $conn = new DatabaseConnection();
    
            $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);
    
            try
            {
                $result = $conn->getHandle()->prepare("SELECT * FROM calendars WHERE calendarId=?");
                $r = $result->execute([$calId]);
    
                $res = $result->fetchAll();
    
                //Construct new Obj
                $obj = new Calendar();
                $obj->calendarId = $res[0]['calendarId'];
                $obj->title = $res[0]['title'];
                $obj->owner = User::getFromId($res[0]['ownerId'])
                $obj->priority = $res[0]['priority'];
                $obj->fetchEvents();
                
                //$obj->users; FETCH
                

                return $obj;
            }
            catch(PDOException $e)
            {
                //Nothing to do here
            }
        }

        //Ctor.
        private function __construct() 
        {

        }

        //Get Id
        public function getId()
        {
            return $this->calendarId;
        }

        //Get Id
        public function setId($newId)
        {
            $this->calendarId = $newId;
        }
        
        //Get Title
        public function getTitle()
        {
            return $this->title;
        }

        //Set Title
        public function setTitle($newTitle)
        {
            $this->title = $newTitle;
        }

        //Get Owner
        public function getOwner()
        {
            return $this->owner;
        }

        //Set Owner
        public function setOwner($newOwner)
        {
            $this->owner = $newOwner;
        }

        //Get Priority
        public function getPriority()
        {
            return $this->priority;
        }

        //Set Priority
        public function setPriority($newP)
        {
            $this->priority = $newP;
        }

        //Serialize to JSON
        public function jsonSerialize()
        {
            return 
            [
                "calendarID" => $this->calendarId,
                "title"  => $this->title,
                "priority" => $this->priority,
                "owner" => json_encode($this->owner),
                "events" => json_encode($this->events),
                "users" => json_encode($this->users)
            ];
        }
        
        //Fetch Events
        public function fetchEvents()
        {
            $this->events = array();

            $conn = new DatabaseConnection();

            $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

            try
            {
                $result = $conn->getHandle()->prepare("SELECT * FROM events WHERE calendarId=?");
                $r = $result->execute([$this->calendarId]);     

                $res = $result->fetchAll();


                foreach ($res as $row) 
                {
                    array_push($this->events, Event::getEvent($row['eventId']));
                }
            }
            catch(PDOException $e)
            {
                //Nothing
            }
        }

        }

        //Fetch users with table access
        public function fetchUsers()
        {
            $this->users = array();

            $conn = new DatabaseConnection();

            $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

            try
            {
                $result = $conn->getHandle()->prepare("SELECT * FROM userCalendar WHERE calendarId=?");

                $r = $result->execute([$this->calendarId]);

                $res = $result->fetchAll();

                foreach ($res as $row) 
                {
                    array_push($this->users, User::getFromId($row['userId']));
                }
            }
            catch(PDOException $e)
            {
                //Nothing
            }
        }

        //ADD USER, ADD EVE, PERMS, HAS EVENT, REM EVENT, MOD EVENT

        //Members
        private $calendarId;
        private $title;
        private $owner;
        private $priority;
        private $events;
        private $users;
}

?>