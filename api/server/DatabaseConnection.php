<?php

require_once('config.php');

///
///	PDO Database Connection Class
///
///
class DatabaseConnection
{
	//Connect
	public function connect($host, $user, $password, $database, $port = 3306)
	{
		if($this->connected)
		{
			return;
		}
		
		try
		{
			$this->handle = new PDO("mysql:host=" . $host . ";port=".$port.";dbname=" . $database . ";", $user, $password);
			$this->handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$this->handle->exec("set names utf8");
		}
		catch (PDOException $e)
		{
			echo "Connection error: " . $e->getMessage();
			return;
		}

		$this->connected = true;
	}	

	//Get PDO Handle
	public function getHandle()
	{
		if(!$this->connected)
		{
			return;
		}

		return $this->handle;
	}

	//Disconnect
	public function disconnect()
	{	
		$this->handle = null; 
		$this->connected = false;
	}

	//Begin Transaction
	public function beginTransaction()
	{
		if(!$this->connected)
		{
			return;
		}

		$this->handle->beginTransaction();
	}

	//Commit
	public function commit()
	{
		if(!$this->connected)
		{
			return;
		}

		$this->handle->commit();
	}
	
	//Rollback
	public function rollback()
	{
		if(!$this->connected)
		{
			return;
		}

		$this->handle->rollBack();
	}

	private $connected = false;
	private $handle = null;
}

?>