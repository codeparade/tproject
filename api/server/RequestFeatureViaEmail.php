<?php

require_once("IRequestFeature.php");
require_once("config.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpm/Exception.php';
require 'phpm/PHPMailer.php';
require 'phpm/SMTP.php';

class RequestFeatureViaEmail implements IRequestFeature
{
    //Request Feature
    public function sendFeatureRequest($nickname, $desc, $email)
    {
        $mail = new PHPMailer(true);   

        try {
            //Server settings
            $mail->SMTPDebug = 4;
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'quoted-printable';    
            $mail->Debugoutput = function($str, $level) { error_log($str, 0);};                             
            $mail->isSMTP();                                      
            $mail->Host = Config::$smtpHost; 
            $mail->SMTPAuth = true;                              
            $mail->Username = Config::$smtpUser;                 
            $mail->Password = Config::$smtpPasswd;      
            $mail->SMTPSecure = 'tls';
            $mail->Port = Config::$smtpPort;

            //Recipients
            $mail->setFrom(Config::$smtpUser, Config::$smtpUser);
            $mail->addAddress(Config::$featureReceiver); 

            //Content
            $mail->isHTML(true);                                
            $mail->Subject = "New Feature Request";
            $mail->Body    = "User:".$nickname." (".$email.") Requested new feature:\r\n".$desc;
            $mail->AltBody = "User:".$nickname." (".$email.") Requested new feature:\r\n".$desc;
            $mail->send();

        } catch (Exception $e) {

        }
    }

    //Send Confirm
    public function sendFeatureRequestConfirmation($nickname, $desc, $email)
    {
       $mail = new PHPMailer(true);   

        try {
            //Server settings
            $mail->SMTPDebug = 4;
            $mail->CharSet = 'UTF-8';
            $mail->Encoding = 'quoted-printable';    
            $mail->Debugoutput = function($str, $level) { error_log($str, 0);};                             
            $mail->isSMTP();                                      
            $mail->Host = Config::$smtpHost; 
            $mail->SMTPAuth = true;                              
            $mail->Username = Config::$smtpUser;                 
            $mail->Password = Config::$smtpPasswd;      
            $mail->SMTPSecure = 'tls';
            $mail->Port = Config::$smtpPort;

            //Recipients
            $mail->setFrom(Config::$smtpUser, Config::$smtpUser);
            $mail->addAddress($email); 

            //Content
            $mail->isHTML(true);                                
            $mail->Subject = "New Feature Request";
            $mail->Body    = "Hi ".$nickname." (".$email.") Thanks for requesting new feature:\r\n".$desc;
            $mail->AltBody = "Hi ".$nickname." (".$email.") Thanks for requesting new feature:\r\n".$desc;
            $mail->send();

        } catch (Exception $e) {
        }
        
    }
}

?>