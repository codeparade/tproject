<?php

require_once("DatabaseConnection.php");

//We need to Implement JSONSerializable iface due to problem with JSON_ENCODE
class Event implements JsonSerializable
{
    //Gets Event from Id
    public static function getEvent($evId)
    {
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM events WHERE eventId=?");
            $r = $result->execute([$evId]);     
            
            $r = $result->fetchAll();
            
            $obj = new Event();
            $obj->eventId = $eventId;
            $obj->title = $r[0]['title'];
            $obj->description = $r[0]['description'];
            $obj->dateTime = $r[0]['dateTime'];
            $obj->color = $r[0]['color'];

            return $obj;
		}
		catch(PDOException $e)
		{
            //Nothing
        }
    }

    //Gets ID
    public function getId()
    {
        return $this->eventId;
    }

    //Sets ID
    public function setID($newId)
    {
        $this->eventId = $newId;
    }

    //Gets Title
    public function getTitle()
    {
        return $this->$title;
    }

    //Sets Title
    public function setTitle($newTitle)
    {
        $this->$title = $newTitle;
    }

    //Gets Description
    public function getDescription()
    {
        return $this->$description;
    }

    //Sets Description
    public function setDescription($newDesc)
    {
        $this->$description = $newDesc;
    }

    //Gets color
    public function getColor()
    {
        return $this->$color;
    }
    
    //Sets color
    public function setColor($newColor)
    {
        $this->$color = $newColor;
    }

    //Gets datetime
    public function getDatetime()
    {
        return $this->$dateTime;
    }

    //Sets Datetime
    public function setDateTime($newDateTime)
    {
        $this->$dateTime = $newDateTime;
    }

    //JSON Serialize
    public function jsonSerialize()
    {
        return [
            "title" => $this->title,
            "desc"  => $this->descriprion,
            "datetime" => $this->dateTime,
            "color" => $this->color
        ];
    }
        
    //Members
    private $eventId;
    private $title;
    private $description;
    private $color;
    private $dateTime;
    
}

?>