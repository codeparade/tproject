<?php

require_once("User.php");

//We need to Implement JSONSerializable iface due to problem with JSON_ENCODE
class Task  implements JsonSerializable
{
    //Gets Task from Id
    public static function getTask($taskId, $idForL = 0)
    {
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        try
		{
            $result = $conn->getHandle()->prepare("SELECT * FROM tasks WHERE taskId=?");
            $r = $result->execute([$taskId]);     
            
            $r = $result->fetchAll();
            
            $obj = new Task();
            $obj->taskId = $taskId;
            $obj->name = $r[0]['name'];
            $obj->desc = $r[0]['description'];
            $obj->deadline = $r[0]['deadline'];
            $obj->color = $r[0]['color'];
            $obj->creator = User::getFromId($res[0]['creatorId']);
            $obj->idForLukasz = $idForL;

            return $obj;
		}
		catch(PDOException $e)
		{
            //Nothing
        }
    }

    //Gets ID
    public function getId()
    {
        return $this->taskId;
    }

    //Sets ID
    public function setId($id)
    {
        $this->taskId = $id;
    }

    //Gets Name
    public function getName()
    {
        return $this->name;
    }
    
    //Sets Name
    public function setName($name)
    {
        $this->name = $name;
    }

    //Gets desc
    public function getDesc()
    {
        return $this->desc;
    }

    //Sets Desc
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }

    //Gets Deadline
    public function getDeadline()
    {
        return $this->deadline;
    }

    //Sets Deadline
    public function setDeadline($deadL)
    {
        $this->deadline = $deadL;
    }

    //Gets Color
    public function getColor()
    {
        return $this->color;
    }

    //Sets color
    public function setColor($col)
    {
        $this->color = $col;
    }

    //Gets Task Creator
    public function getCreator()
    {
        return $this->creator;
    }

    //Sets Task Creator
    public function setPosition($pos)
    {
        $this->taskPos = $pos;
    }

    //Gets Task Creator
    public function getPosition()
    {
        return $this->taskPos;
    }

    //Sets Task Creator
    public function setCreator($user)
    {
        $this->creator = $user;
    }

    //JSON Serialize
    public function jsonSerialize()
    {
        return [
            "taskId" => $this->taskId,
            "name" => $this->name,
            "desc"  => $this->desc,
            "deadline" => $this->deadline,
            "color" => $this->color,
            "position" => $this->taskPos,
            "id" => 'a'.strval($this->taskId)
        ];
    }

    //Members
    private $taskId;
    private $name;
    private $desc;
    private $deadline;
    private $color;
    private $creator;
    private $taskPos;
    private $idForLukasz;
}
?>