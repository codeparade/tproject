<?php

//Interface for requesting new features
interface IRequestFeature
{
    //Request Feature
    public function sendFeatureRequest($nickname, $desc, $email);

    //Send Confirm
    public function sendFeatureRequestConfirmation($nickname, $desc, $email);
}


?>