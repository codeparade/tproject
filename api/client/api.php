<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../server/config.php';

require_once('../server/SessionManager.php');
require_once('../server/UserManager.php');
require_once('../server/Table.php');
require_once('../server/CurrentUser.php');
require_once('../server/RequestFeatureViaEmail.php');

$app = new \Slim\App;

//Response
$response = array(
    "code" => 0,
    "session" => null,
    "data" => "Internal Error"
);

//Nothing to do -> Must be for handling browser requests
$app->get('/', function (Request $request, Response $responsse, array $args)
{
    $response['data'] = "Nothing to do";
    echo JSON_ENCODE($response);
});



/////////////////////////////////////////
///   USERS
/////////////////////////////////////////

//Login User
$app->post('/login', function (Request $request, Response $responsse, array $args)
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['username']) || !isset($parsedBody['password']))
    {
        $response['data'] = "No login on password sent";
        echo JSON_ENCODE($response);
        die();
    }

    //Validate User
    try
    {
        $vald = UserManager::validateUser(strtolower($parsedBody['username']), $parsedBody['password']);
   
        if($vald === false)
        {
            $response['data'] = "Invalid Credentials";
            echo JSON_ENCODE($response);
            die();
        }

        //Create session
        $sess = SessionManager::registerNewSession($vald['userId'], $vald['showname']);

        $response['session'] = $sess->toJSON();
        $response['code'] = 1;
        $response['data'] = $vald;

        //Insert into logs
        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

        //Return data
        echo JSON_ENCODE($response, JSON_UNESCAPED_UNICODE);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Unknown Error";
        echo JSON_ENCODE($response);
        die();
    }

});

//Logout User
$app->post('/logout', function (Request $request, Response $responsse, array $args)
 {
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    try
    {

        $res = SessionManager::validateSession($parsedBody['session'], false);

        if($res !== true)
        {
            throw new Exception("Cant validate sess");
        }

        $sessionData = stripslashes($parsedBody['session']);

		$dt = SessionInfo::fromJSON($sessionData);
        SessionManager::destroySession($dt->sessionId);

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Invalid session ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Validate User Session
$app->post('/validateSession', function (Request $request, Response $responsse, array $args)
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    try
    {
        $sess = SessionManager::validateSession($parsedBody['session']);
        
        $response['code'] = 1;
        $response['session'] = $sess->toJSON();
        $response['data'] = 'OK';
        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Change Password
$app->post('/changePwd', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['oldPw']) || !isset($parsedBody['newPwd']) || !isset($parsedBody['newPwd2']))
    {
        $response['data'] = "No password sent";
        echo JSON_ENCODE($response);
        die();
    }

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        if($parsedBody['newPwd'] !== $parsedBody['newPwd2'])
        {
            throw new Exception("Passwords are not identical");
        }
         
        $vald = UserManager::changeUserPasswd($uid, $parsedBody['newPwd'], $parsedBody['oldPw']);

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Register User
$app->post('/register', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['username']) || !isset($parsedBody['showname']) || !isset($parsedBody['firstname']) || !isset($parsedBody['lastname']) || !isset($parsedBody['password']) || !isset($parsedBody['password2']) || !isset($parsedBody['email']))
    {
    $response['data'] = "Invalid data sent";
    echo JSON_ENCODE($response);
    die();
    }

    if($parsedBody['password'] !== $parsedBody['password2'])
    {
        $response['data'] = "Passwords doesnt match";
        echo JSON_ENCODE($response);
        die();
    }

    $pwd = password_hash($parsedBody['password'], PASSWORD_BCRYPT);

    $conn = new DatabaseConnection();

    $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);

    try
		{
            //Call Proc
            $result = $conn->getHandle()->prepare("CALL registerUsr(?,?,?,?,?,?,?)");
            $r = $result->execute([trim($parsedBody['username']), trim($parsedBody['showname']), $parsedBody['firstname'], $parsedBody['lastname'], $parsedBody['email'], $pwd, $_SERVER['REMOTE_ADDR']]);    
         
            if($r == false)
            {
                //Recv error from SQL
                $response['data'] = $result->errorInfo()[2];
                echo JSON_ENCODE($response);
                die();
                
            }
            $response['data'] = "OK";
            $response['code'] = 1;
            echo JSON_ENCODE($response);
            die();
		}
		catch(PDOException $e)
		{
            $response['data'] = "Failed: ".$e->getMessage();
            echo JSON_ENCODE($response);
            die();
		}
});


/////////////////////////////////////////
///   TABLES
/////////////////////////////////////////

//Add New Table
$app->post('/tables/addTable', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['title']) || !isset($parsedBody['priority']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);
    
        try
            {
                //Call Proc
                $result = $conn->getHandle()->prepare("INSERT INTO tables VALUES(NULL,?,?,?, NULL)");
                $r = $result->execute([$parsedBody['title'], $parsedBody['priority'], $uid]);    
                
                if($r == false)
                {
                    //Recv error from SQL
                    $response['data'] = $result->errorInfo()[2];
                    echo JSON_ENCODE($response);
                    die();
                    
                }

                $result = $conn->getHandle()->prepare("INSERT INTO userTable VALUES(?,?)");
                $r = $result->execute([$uid, $conn->getHandle()->lastInsertId()]);   

                $response['data'] = "OK";
                $response['code'] = 1;
                echo JSON_ENCODE($response);
                die();
            }
            catch (PDOException $e)
            {
                echo "err";
            }
    

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Remove Table
$app->post('/tables/removeTable', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['tabId']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);
    
        try
            {
                //Call Proc
                $result = $conn->getHandle()->prepare("DELETE FROM tables WHERE tabId=? AND ownerId=?");
                $r = $result->execute([$parsedBody['tabId'],  $uid]);    
             
                $result = $conn->getHandle()->prepare("DELETE FROM userTable WHERE tabId=?");
                $r = $result->execute([$parsedBody['tabId']]);  

                $response['data'] = "OK";
                $response['code'] = 1;
                echo JSON_ENCODE($response);
                die();


            }
            catch (PDOException $e)
            {
                echo "err";
            }
    

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response); 
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Get Current Logged In User Tables
$app->post('/tables/getUserTables', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        CurrentUser::getInstance()::parseFromSession($parsedBody['session']);

        $response['code'] = 1;
        $response['data'] = Table::getUserTables();

        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Add task
$app->post('/tables/addTask', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['name']) || !isset($parsedBody['desc']) || !isset($parsedBody['deadline']) || !isset($parsedBody['tabId']) || !isset($parsedBody['color']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        CurrentUser::getInstance()::parseFromSession($parsedBody['session']);
        $tab = Table::getTable($parsedBody['tabId']);

        $task = new Task();
        $task->setName($parsedBody['name']);
        $task->setDesc($parsedBody['desc']);
        $task->setDeadline(date('Y-m-d h:i:s', strtotime($parsedBody['deadline'])));
        $task->setColor($parsedBody['color']);
        $task->setCreator(CurrentUser::getInstance());


        $tab->addTask($task);

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response);
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Remove Task
$app->post('/tables/removeTask', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['taskId']) || !isset($parsedBody['tabId']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    CurrentUser::getInstance()::parseFromSession($parsedBody['session']);

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $tbl = Table::getTable($parsedBody['tabId']);

        if(!$tbl->hasUserAccess(CurrentUser::getInstance()))
        {
            $response['data'] = "Access denied";
            echo JSON_ENCODE($response);
            die();
        }

        $conn = new DatabaseConnection();

        $conn->connect(Config::$dbHost, Config::$dbUser, Config::$dbPass, Config::$dbName, Config::$dbPort);
    
        try
            {
                //Call Proc
                $result = $conn->getHandle()->prepare("DELETE FROM tasks WHERE taskId=?");
                $r = $result->execute([$parsedBody['taskId']]);    
             
                if($r == false)
                {
                    //Recv error from SQL
                    $response['data'] = $result->errorInfo()[2];
                    echo JSON_ENCODE($response);
                    die();
                    
                }

                $response['data'] = "OK";
                $response['code'] = 1;
                echo JSON_ENCODE($response);
                die();
            }
            catch (PDOException $e)
            {
                echo "err";
            }
    

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response); 
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Modify Task
$app->post('/tables/modifyTask', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['taskId']) || !isset($parsedBody['tabId']) || !isset($parsedBody['action']) || !isset($parsedBody['newData']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    CurrentUser::getInstance()::parseFromSession($parsedBody['session']);

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $tbl = Table::getTable($parsedBody['tabId']);

        if(!$tbl->hasUserAccess(CurrentUser::getInstance()))
        {
            $response['data'] = "Access denied";
            echo JSON_ENCODE($response);
            die();
        }

        $tbl->modifyTask($parsedBody['taskId'], $parsedBody['action'], $parsedBody['newData']);

        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response); 
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

/////////////////////////////////////////
///  TESTOWE!!!!!!!!!!!
/////////////////////////////////////////

///DOWN: TEST AND MAKE API REF

//Modify Table
$app->post('/tables/modifyTable', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['tabId']) || !isset($parsedBody['action']) || !isset($parsedBody['newData']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    CurrentUser::getInstance()::parseFromSession($parsedBody['session']);

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $tbl = Table::getTable($parsedBody['tabId']);

        if(!$tbl->isUserOwner(CurrentUser::getInstance()))
        {
            $response['data'] = "Access denied";
            echo JSON_ENCODE($response);
            die();
        }

        //Query to DB
        try
            {
                $stmt = "";
                if($parsedBody['action'] == "NAME")
                {
                    $stmt = "UPDATE tables SET title=? WHERE tabId=?";
                }
                else
                {
                    $stmt = "UPDATE tables SET bgUrl=? WHERE tabId=?";
                }
                //Call Proc
                $result = $conn->getHandle()->prepare($stmt);

                $r = $result->execute([$parsedBody['newData'], $parsedBody['tabId']]);    
             
                if($r == false)
                {
                    //Recv error from SQL
                    $response['data'] = $result->errorInfo()[2];
                    echo JSON_ENCODE($response);
                    die();
                    
                }

                $response['data'] = "OK";
                $response['code'] = 1;
                echo JSON_ENCODE($response);
                die();
            }
            catch (PDOException $e)
            {
                echo "err";
            }
     
        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response); 
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

//Save Tasks pos to db
$app->post('/tables/updatePos', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();

    if(!isset($parsedBody['session']))
    {
        $response['data'] = "No session sent";
        echo JSON_ENCODE($response);
        die();
    }

    if(!isset($parsedBody['tabId']) || !isset($parsedBody['data']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    CurrentUser::getInstance()::parseFromSession($parsedBody['session']);

    //validate Session and make changes
    try
    {
        $res = SessionManager::validateSession($parsedBody['session'], false, true);

        $uid = $res->sessionOwner;

        $tbl = Table::getTable($parsedBody['tabId']);

        if(!$tbl->hasUserAccess(CurrentUser::getInstance()))
        {
            $response['data'] = "Access denied";
            echo JSON_ENCODE($response);
            die();
        }

        //Query to DB
        try
            {
                $result = $conn->getHandle()->prepare("UPDATE tasks SET taskPos=? WHERE taskId=?");


                foreach(JSON_DECODE($parsedBody['data']) as $pair)
                {
                    $r = $result->execute([$pair["pos"], $pair["taskId"]]);     
                }


                $response['data'] = "OK";
                $response['code'] = 1;
                echo JSON_ENCODE($response);
                die();
            }
            catch (PDOException $e)
            {
                echo "err";
            }
     
        $response['code'] = 1;
        $response['data'] = "OK";
        echo JSON_ENCODE($response); 
        die();
    }
    catch(Exception $e)
    {
        $response['data'] = "Error ".$e->getMessage();
        echo JSON_ENCODE($response);
        die();
    }
});

/////////////////////////////////////////
///   CALENDARS
/////////////////////////////////////////


/////////////////////////////////////////
///   FEATURES
/////////////////////////////////////////

$app->post('/requestFeature', function (Request $request, Response $responsse, array $args) 
{
    $parsedBody = $request->getParsedBody();


    if(!isset($parsedBody['nick']) || !isset($parsedBody['mail']) || !isset($parsedBody['desc']))
    {
        $response['data'] = "No data sent";
        echo JSON_ENCODE($response);
        die();
    }

    $how = null;
    if(Config::$howFeatureWorks == 'email')
    {
        $how = new RequestFeatureViaEmail();
    }

    $how->sendFeatureRequest($parsedBody['nick'], $parsedBody['desc'], $parsedBody['mail']);
    $how->sendFeatureRequestConfirmation($parsedBody['nick'], $parsedBody['desc'], $parsedBody['mail']);

    $response['code'] = 1;
    $response['data'] = "OK";
    echo JSON_ENCODE($response);
    die();
});

$app->run();
