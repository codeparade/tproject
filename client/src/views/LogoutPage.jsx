import React, { Component } from "react";
import { Layout } from "antd";
import "antd/dist/antd.css";
import Cookies from 'js-cookie';
import { Redirect } from "react-router-dom";

const { Content } = Layout;

class LogoutPage extends Component {
  state = {
    loggedOut: false
  }

  componentDidMount() {
    fetch("https://apitproject.promeger.com/logout", {
      method: "POST",
      body: JSON.stringify({
        session: Cookies.get("sessionData")
      }),
      crossOrigin: true,
      headers: {
        "Content-Type": "application/json"
      }})
      .then(res => res.json())
      .then(res => {
        console.log(res);
        Cookies.remove("sessionData");
        this.setState({
          loggedOut: true
        })
      })
      .catch(err => console.log("Catched in logout.", err));
  }

  render() {
    if (this.state.loggedOut) {
      return <Redirect to="/" />;
    }

    return (
      <Content>
        <div
          style={{
            padding: 24,
            background: "#fff",
            minHeight: 400,
            borderTop: "5px solid #001529"
          }}
        >
          You have been logged out.
        </div>
      </Content>
    );
  }
}

export default LogoutPage;
