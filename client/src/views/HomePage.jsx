import React, { Component } from "react";
import { Layout } from "antd";
import "antd/dist/antd.css";

const { Content } = Layout;

class HomePage extends Component {
  render() {
    return (
      <Content>
        <div
          style={{
            padding: 24,
            background: "#fff",
            minHeight: 400,
            borderTop: "5px solid #001529"
          }}
        >
          Content
        </div>
      </Content>
    );
  }
}

export default HomePage;
