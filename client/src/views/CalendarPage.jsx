import React, { Component } from "react";
import { Layout } from "antd";
import "antd/dist/antd.css";
import Calendar from 'react-calendar';
import "../styles.css";
import Cookies from 'js-cookie';
import { Redirect } from "react-router-dom";


const { Content } = Layout;

class CalendarPage extends Component {
  render() {
    if (!Cookies.get("sessionData")) {
      return <Redirect to="/login" />
    }

    return (
      <Content>
        <div
          style={{
            padding: 24,
            background: "#fff",
            minHeight: 400,
            borderTop: "5px solid #001529"
          }}
        >
          <Calendar className={'calendar'} />
        </div>
      </Content>
    );
  }
}

export default CalendarPage;
