import React from "react";
import { Form, Icon, Input, Button, Modal } from "antd";
import { Redirect } from 'react-router-dom';

const { TextArea } = Input;

class NormalNewFunctionalityForm extends React.Component {
  state = {
    ModalText: "Content of the modal",
    visible: false,
    confirmLoading: false,
    toLoginPage: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
        toLoginPage: true
      });

    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", JSON.stringify(values));

        fetch("https://apitproject.promeger.com/requestFeature", {
          method: "POST",
          body: JSON.stringify(values),
          dataType: "jsonp",
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(res => {
            console.log(res);
            if (res.data === "OK") {
              this.setState({
                ModalText: "Your proposition has been sent! Thank you"
              });
            } else {
              this.setState({
                ModalText: res.data
              });
            }

            this.showModal();
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, confirmLoading, ModalText } = this.state;

    if (this.state.toLoginPage === true) {
      return <Redirect to='/' />
    }

    return (
      <div
        style={{
          padding: 24,
          background: "#fff",
          minHeight: 400,
          borderTop: "5px solid #001529"
        }}
      >
        <Modal
          title="Info"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <p>{ModalText}</p>
        </Modal>
        <Form
          onSubmit={this.handleSubmit}
          className="login-form"
          style={{ maxWidth: 400 }}
        >
          <Form.Item>
            {getFieldDecorator("nick", {
              rules: [
                {
                  required: true,
                  message: "Please enter your nickname!"
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Nickname"
                name="nick"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("desc", {
              rules: [
                {
                  required: true,
                  message: "Enter description of your feature!"
                }
              ]
            })(
              <TextArea rows={4} placeholder="Feature description..." name="desc" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("mail", {
              rules: [{ required: true, message: "Please input your E-Mail!" }]
            })(
              <Input
                prefix={
                  <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="E-Mail"
                name="mail"
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Send 
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const WrappedNormalNewFunctionalityForm = Form.create({ name: "normal_new_functionality" })(
  NormalNewFunctionalityForm
);

export default WrappedNormalNewFunctionalityForm;
