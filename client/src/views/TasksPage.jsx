import React, { Component } from "react";
import { Layout, Button, Modal, Form, DatePicker, TimePicker } from "antd";
import "antd/dist/antd.css";
import Column from "../components/Column";
import { DragDropContext } from "react-beautiful-dnd";
import Styled from "styled-components";
import { Redirect } from "react-router-dom";
import AddTable from "../components/AddTable";
import Cookies from "js-cookie";

const ConfigPanel = Styled.div`
  margin: 8px;
  border-radius: 10px;
  padding: 8px;
  background: #02284b;
  color: #8798a1;
`;

const { Content } = Layout;

class TasksPage extends Component {
  cnt = 1;

  state = {
    size: "small",
    visible: false,
    confirmLoading: false,
    refresh: false
    // tasks: {
    //   "task-1": { id: "task-1", content: "Test1" },
    //   "task-2": { id: "task-2", content: "Test2" },
    //   "task-3": { id: "task-3", content: "Test3" },
    //   "task-4": { id: "task-4", content: "Test4" },
    //   "task-41": { id: "task-41", content: "Test41" },
    //   "task-42": { id: "task-42", content: "Test42" },
    //   "task-43": { id: "task-43", content: "Test43" },
    //   "task-44": { id: "task-44", content: "Test44" }
    // },
    // columns: {
    //   0: {
    //     id: "column-1",
    //     title: "To do",
    //     tasks: ["task-1", "task-2", "task-3", "task-4"]
    //   },
    //   1: {
    //     id: "column-2",
    //     title: "To do2",
    //     tasks: ["task-41", "task-42", "task-43", "task-44"]
    //   }
    // },
    // columnOrder: [0, 1, 2]
  };

  componentWillMount() {
    //console.log(Cookies.get("sessionData"));
    fetch("https://apitproject.promeger.com/tables/getUserTables", {
      method: "POST",
      body: JSON.stringify({
        session: Cookies.get("sessionData")
      }),
      crossOrigin: true,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        console.log(res.data);
        // console.log(this.state.columns);
        this.setState({
          columns: res.data,
          columnOrder: Object.keys(res.data)
        });
      })
      .catch(err => console.log("Catched in tasks.", err));
  }

  onDragEnd = res => {
    // const { destination, source, draggableId } = res;
    // console.log(draggableId);
    // if (!destination) {
    //   return;
    // }
    // if (
    //   destination.droppableId === source.droppableId &&
    //   destination.index === source.index
    // ) {
    //   return;
    // }
    // let newState = {};
    // if (destination.droppableId === source.droppableId) {
    //   const column = this.state.columns[source.droppableId];
    //   const newTasks = Array.from(column.tasks);
    //   newTasks.splice(source.index, 1);
    //   newTasks.splice(destination.index, 0, draggableId);
    //   const newColumn = {
    //     ...column,
    //     tasks: newTasks
    //   };
    //   newState = {
    //     ...this.state,
    //     columns: {
    //       ...this.state.columns,
    //       [newColumn.id]: newColumn
    //     }
    //   };
    // } else {
    //   const column_to = this.state.columns[destination.droppableId];
    //   const column_from = this.state.columns[source.droppableId];
    //   const newTasks_to = Array.from(column_to.tasks);
    //   const newTasks_from = Array.from(column_from.tasks);
    //   newTasks_from.splice(source.index, 1);
    //   newTasks_to.splice(destination.index, 0, draggableId);
    //   const newColumn_to = {
    //     ...column_to,
    //     tasks: newTasks_to
    //   };
    //   const newColumn_from = {
    //     ...column_from,
    //     tasks: newTasks_from
    //   };
    //   newState = {
    //     ...this.state,
    //     columns: {
    //       ...this.state.columns,
    //       [newColumn_to.id]: newColumn_to,
    //       [newColumn_from.id]: newColumn_from
    //     }
    //   };
    // }
    // this.setState(newState);
  };

  IsJsonString = str => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
        refresh: true
      });
    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  render() {
    const { visible, confirmLoading, size } = this.state;

    if (!Cookies.get("sessionData")) {
      return <Redirect to="/login" />
    }

    if (this.state.refresh === true) {
      return <Redirect to="/tasks" />;
    }

    return (
      <Content>
        <div
          style={{
            padding: 24,
            background: "#fff",
            minHeight: 400,
            borderTop: "5px solid #001529"
          }}
        >
          <ConfigPanel>
            <Button
              type="primary"
              shape="circle"
              icon="plus"
              size={size}
              onClick={() => this.showModal()}
            />
          </ConfigPanel>

          <Modal
            title="Add table"
            visible={visible}
            onOk={this.handleOk}
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
            footer={[]}
          >
            <AddTable />
          </Modal>

          {this.state.columns && (
            <DragDropContext onDragEnd={this.onDragEnd}>
              {this.state.columnOrder.map(columnId => {
                let column = this.state.columns[columnId];

                if (this.IsJsonString(column.tasks)) {
                  column.tasks = JSON.parse(column.tasks);
                }

                column.id = columnId;
                console.log("tetrererer", column.tasks);

                if (Object.keys(column.tasks).length > 0) {
                  let tasks = Object.keys(column.tasks).map(task => {
                    console.log(task);
                    column.tasks[task].ids = task;
                    return column.tasks[task];
                  });
                  console.log(tasks);

                  return (
                    <Column key={column.taskId} column={column} tasks={tasks} />
                  );
                } else {
                  const tasks = [
                    {
                      ids: this.cnt++,
                      desc: "No tasks",
                      color: "#000000"
                    }
                  ];
                  return (
                    <Column key={column.taskId} column={column} tasks={tasks} />
                  );
                }
              })}
            </DragDropContext>
          )}
        </div>
      </Content>
    );
  }
}

export default TasksPage;
