import React from "react";
import { Form, Icon, Input, Button, Checkbox, Modal } from "antd";
import { Link, Redirect } from "react-router-dom";
import Cookies from "js-cookie";
import Base64 from "js-base64";

class NormalLoginForm extends React.Component {
  state = {
    ModalText: "Content of the modal",
    visible: false,
    confirmLoading: false,
    refresh: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
        refresh: true
      });
    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);

        //const data = new FormData(e.target);

        fetch("https://apitproject.promeger.com/login", {
          method: "POST",
          body: JSON.stringify(values),
          crossOrigin: true,
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(res => {
            console.log(res);
            if (res.code === 1) {
              let d = new Date();
              d.setTime(d.getTime() + 30 * 60 * 1000 * 10);

              Cookies.set("sessionData", res.session, {
                path: "/",
                expires: d
              });

              // Cookies.set("cache", Base64.encode(res.data), {
              //   path: "/",
              //   expires: d
              // });

              this.setState({
                ModalText: "You have been logged in!"
              });
            } else {
              this.setState({
                ModalText: res.data
              });
            }

            this.showModal();
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, confirmLoading, ModalText } = this.state;

    if (Cookies.get("sessionData")) {
      return <Redirect to="/tasks" />
    }

    if (this.state.refresh === true) {
      return <Redirect to="/" />;
    }

    return (
      <div
        style={{
          padding: 24,
          background: "#fff",
          minHeight: 400,
          borderTop: "5px solid #001529"
        }}
      >
        <Modal
          title="Info"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <p>{ModalText}</p>
        </Modal>
        <Form
          onSubmit={this.handleSubmit}
          className="login-form"
          style={{ maxWidth: 400 }}
        >
          <Form.Item>
            {getFieldDecorator("username", {
              rules: [
                { required: true, message: "Please input your username!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Username"
                name="username"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Please input your Password!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
                name="password"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("remember", {
              valuePropName: "checked",
              initialValue: true
            })(<Checkbox>Remember me</Checkbox>)}
            <br />
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Log in
            </Button>
            <br />
            <a className="login-form-forgot" href="">
              Forgot password
            </a>
            <br />
            Or <Link to="/register">register now!</Link>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  NormalLoginForm
);

export default WrappedNormalLoginForm;
