import React from "react";
import { Form, Icon, Input, Button, Modal } from "antd";
import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

class NormalRegisterForm extends React.Component {
  state = {
    ModalText: "Content of the modal",
    visible: false,
    confirmLoading: false,
    toLoginPage: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
        toLoginPage: true
      });

    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", JSON.stringify(values));

        //const data = new FormData(e.target);

        fetch("https://apitproject.promeger.com/register", {
          method: "POST",
          body: JSON.stringify(values),
          dataType: "jsonp",
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(res => {
            console.log(res);
            if (res.data === "OK") {
              this.setState({
                ModalText: "You have been registered! Now you can log in!"
              });
            } else {
              this.setState({
                ModalText: res.data
              });
            }

            this.showModal();
          });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, confirmLoading, ModalText } = this.state;

    if (Cookies.get("sessionData")) {
      return <Redirect to="/tasks" />
    }

    if (this.state.toLoginPage === true) {
      return <Redirect to='/login' />
    }

    return (
      <div
        style={{
          padding: 24,
          background: "#fff",
          minHeight: 400,
          borderTop: "5px solid #001529"
        }}
      >
        <Modal
          title="Info"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
        >
          <p>{ModalText}</p>
        </Modal>
        <Form
          onSubmit={this.handleSubmit}
          className="login-form"
          style={{ maxWidth: 400 }}
        >
          <Form.Item>
            {getFieldDecorator("username", {
              rules: [
                {
                  required: true,
                  message: "Please input your username used to logging to!"
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Username"
                name="username"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("showname", {
              rules: [
                {
                  required: true,
                  message: "Please input nickname visible to others!"
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Shown Name"
                name="showname"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("firstname", {
              rules: [{ required: true, message: "Please input your name!" }]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Name"
                name="firstname"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("lastname", {
              rules: [{ required: true, message: "Please input your surname!" }]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="Surname"
                name="lastname"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password", {
              rules: [
                { required: true, message: "Please input your Password!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
                name="password"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("password2", {
              rules: [{ required: true, message: "Confirm your Password!" }]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
                name="password2"
              />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("email", {
              rules: [{ required: true, message: "Please input your E-Mail!" }]
            })(
              <Input
                prefix={
                  <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="E-Mail"
                name="email"
              />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Sign Up
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const WrappedNormalRegisterForm = Form.create({ name: "normal_register" })(
  NormalRegisterForm
);

export default WrappedNormalRegisterForm;
