import React, { Component } from "react";
import { Layout, Icon } from "antd";
import HomePage from "./views/HomePage";
import LogoutPage from "./views/LogoutPage";
import MenuC from "./components/MenuC";
import FooterC from "./components/FooterC";
import WrappedNormalLoginForm from "./views/LoginPage";
import WrappedNormalRegisterForm from "./views/RegistrationPage";
import { BrowserRouter as Router, Route } from "react-router-dom";
import UserPage from "./views/UserPage";
import TasksPage from "./views/TasksPage";
import CalendarPage from "./views/CalendarPage";
import WrappedNormalNewFunctionalityForm from "./views/NewFunctionalityPage";

const { Sider, Header, Footer } = Layout;

class App extends Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    return (
      <Router>
        <Layout style={{ background: "#fff" }}>
          <Sider
            trigger={null}
            collapsible
            collapsed={this.state.collapsed}
            style={{
              overflow: "auto",
              height: "100vh",
              position: "fixed",
              left: 0
            }}
          >
            <MenuC />
          </Sider>
          <Layout style={this.state.collapsed ? { marginLeft: 80, marginBottom: 100 } : { marginLeft: 200, marginBottom: 100 }}>
            <Header style={{ background: "#fff", paddingLeft: 20 }}>
              <Icon
                className="trigger"
                type={this.props.collapsed ? "menu-unfold" : "menu-fold"}
                onClick={this.toggle}
              />
            </Header> 

            <Route exact path="/" render={() => <HomePage />} />
            <Route exact path="/user" render={() => <UserPage />} />
            <Route exact path="/login" render={() => <WrappedNormalLoginForm />} />
            <Route exact path="/register" render={() => <WrappedNormalRegisterForm />} />
            <Route exact path="/logout" render={() => <LogoutPage />} />
            <Route exact path="/tasks" render={() => <TasksPage />} />
            <Route exact path="/calendar" render={() => <CalendarPage />} />
            <Route exact path="/addfunctionality" render={() => <WrappedNormalNewFunctionalityForm />} />

          </Layout>

          <Footer
              style={{
                position: "fixed",
                bottom: 0,
                width: "100%",
                borderTop: "5px solid #001529",
              }}
            >
              <FooterC />
            </Footer>
        </Layout>
        
      </Router>
    );
  }
}

export default App;
