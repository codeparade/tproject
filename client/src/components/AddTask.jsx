import { Form, DatePicker, Button, Input, Icon } from "antd";
import React from "react";
import Cookies from "js-cookie";
import Styled from "styled-components";

const ConfigPanel = Styled.div`
  margin: 8px;
  border-radius: 10px;
  padding: 8px;
  background: #02284b;
  color: #8798a1;
`;

class AddTaskI extends React.Component {
  state = {
    configPanelStyle: {
      display: "none"
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      // Should format date value before submit.
      let values = {
        ...fieldsValue,
        deadline: fieldsValue["deadline"].format("YYYY-MM-DD HH:mm:ss"),
        tabId: this.props.table,
        color: "#ffffff",
        name: "who cares"
      };
      console.log("Received values of form: ", values);

      values.session = Cookies.get("sessionData");

      values = JSON.stringify(values);

      fetch("https://apitproject.promeger.com/tables/addTask", {
        method: "POST",
        body: values,
        crossOrigin: true,
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(res => {
          console.log(res);
          this.setState({
            info: "Done!",
            configPanelStyle: { display: "block" }
          });
          setTimeout(() => {
            this.setState({
              configPanelStyle: {
                display: "none"
              }
            });
            window.location.reload();
          }, 2000);
        });
        setTimeout(() => {
          this.setState({
            configPanelStyle: {
              display: "none"
            }
          });
          window.location.reload();
        }, 500);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const config = {
      rules: [
        { type: "object", required: true, message: "Please select time!" }
      ]
    };

    return (
      <Form onSubmit={this.handleSubmit}>
        <ConfigPanel style={this.state.configPanelStyle}>
          {this.state.info}
        </ConfigPanel>
        <Form.Item>
          {getFieldDecorator("desc", {
            rules: [
              { required: true, message: "Please input task description!" }
            ]
          })(
            <Input
              prefix={
                <Icon
                  type="double-right"
                  style={{ color: "rgba(0,0,0,.25)" }}
                />
              }
              placeholder="Task description"
              name="desc"
            />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Pick date">
          {getFieldDecorator("deadline", config)(
            <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
          )}
        </Form.Item>
        <Form.Item
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: { span: 16, offset: 8 }
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const AddTask = Form.create({ name: "add_task" })(AddTaskI);
export default AddTask;
