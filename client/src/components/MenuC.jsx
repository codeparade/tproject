import React, { Component } from "react";
import { Menu, Icon } from "antd";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

class MenuC extends Component {
  state = {};
  render() {
    return (
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1">
          <Link to="/user">
            <Icon type="user" />
            <span>Profile</span>
          </Link>
        </Menu.Item>

        <Menu.Item key="2">
          <Link to="/addfunctionality">
            <Icon type="question" />
            <span>Add functionality</span>
          </Link>
        </Menu.Item>

        {!Cookies.get("sessionData") && (
          <Menu.Item key="3">
            <Link to="/login">
              <Icon type="smile" />
              <span>Login</span>
            </Link>
          </Menu.Item>
        )}

        {!Cookies.get("sessionData") && (
          <Menu.Item key="4">
            <Link to="/register">
              <Icon type="user-add" />
              <span>Sign up</span>
            </Link>
          </Menu.Item>
        )}

        {Cookies.get("sessionData") && (
          <Menu.Item key="5">
            <Link to="/tasks">
              <Icon type="logout" />
              <span>Tasks</span>
            </Link>
          </Menu.Item>
        )}

        {Cookies.get("sessionData") && (
          <Menu.Item key="6">
            <Link to="/calendar">
              <Icon type="calendar" />
              <span>Calendar</span>
            </Link>
          </Menu.Item>
        )}

        {Cookies.get("sessionData") && (
          <Menu.Item key="7">
            <Link to="/logout">
              <Icon type="logout" />
              <span>Logout</span>
            </Link>
          </Menu.Item>
        )}
      </Menu>
    );
  }
}

export default MenuC;
