import React, { Component } from "react";
import styled from "styled-components";
import Task from "./Task";
import { Droppable } from "react-beautiful-dnd";
import { Redirect } from "react-router-dom";
import { Modal, Button } from "antd";
import AddTask from "./AddTask";
import Cookies from 'js-cookie';

const Container = styled.div`
  margin: 8px;
  border-radius: 5px;
  background: #02284b;
  color: #8798a1;
`;

const ContainerMenu = styled.div`
  padding: 8px;
  margin-bottom: 8px;
  border-radius: 5px;
  background: #001529;
  color: #fff;
`;

const Title = styled.h3`
  padding: 8px;
  color: #9fa7ae;
`;

const TaskList = styled.div`
  padding: 8px;
`;

class Column extends Component {
  state = {
    size: "small",
    visible: false,
    confirmLoading: false,
    refresh: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({
      confirmLoading: true
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
        refresh: true
      });
    }, 500);
  };

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      visible: false
    });
  };

  removeTable = (tabId) => {
    const values = {
      session: Cookies.get("sessionData"),
      tabId
    };

    fetch("https://apitproject.promeger.com/tables/removeTable", {
      method: "POST",
      body: JSON.stringify(values),
      crossOrigin: true,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        window.location.reload();
      });
  };

  render() {
    const { visible, confirmLoading, size } = this.state;

    if (this.state.refresh === true) {
      return <Redirect to="/tasks" />;
    }

    return (
      <Container>
        <Modal
          title="Add task"
          visible={visible}
          onOk={this.handleOk}
          confirmLoading={confirmLoading}
          onCancel={this.handleCancel}
          footer={[]}
        >
          <AddTask table={this.props.column.tableId} />
        </Modal>
        <Title>
          {this.props.column.title}
          <Button
            type="danger"
            shape="circle"
            icon="close"
            size="small"
            style={{ float: "right" }}
            onClick={() =>
              this.removeTable(this.props.column.tableId)
            }
          />
        </Title>
        <Droppable droppableId={this.props.column.id}>
          {provided => {
            return (
              <TaskList ref={provided.innerRef} {...provided.droppableProps}>
                {this.props.tasks.map((task, index) => (
                  <Task
                    key={task.id}
                    task={task}
                    index={index}
                    tabId={this.props.column.tableId}
                  />
                ))}
                {provided.placeholder}
              </TaskList>
            );
          }}
        </Droppable>
        <ContainerMenu>
          <Button
            type="primary"
            shape="circle"
            icon="plus"
            size={size}
            onClick={() => this.showModal()}
          />
        </ContainerMenu>
      </Container>
    );
  }
}

export default Column;
