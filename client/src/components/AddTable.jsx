import { Form, Button, Input, Icon, InputNumber } from "antd";
import React from "react";
import Cookies from "js-cookie";
import Styled from 'styled-components';

const ConfigPanel = Styled.div`
  margin: 8px;
  border-radius: 10px;
  padding: 8px;
  background: #02284b;
  color: #8798a1;
`;

class AddTableI extends React.Component {
  state = {
    configPanelStyle: {
      display: "none"
    }
  }

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (err) {
        return;
      }

      //values = { values, session: Cookies.get("sessionData") };
      values.session = Cookies.get("sessionData");

      values = JSON.stringify(values);

      console.log("Received values of form: ", values);

      fetch("https://apitproject.promeger.com/tables/addTable", {
        method: "POST",
        body: values,
        crossOrigin: true,
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .then(res => {
          this.setState({ info: "Done!", configPanelStyle: { display: 'block'} });
          setTimeout(() => {
            this.setState({
              configPanelStyle: {
                display: "none"
              }
            });
            window.location.reload();
          }, 2000);
        });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <ConfigPanel style={this.state.configPanelStyle}>
          {this.state.info}
        </ConfigPanel>
        <Form.Item>
          {getFieldDecorator("title", {
            rules: [{ required: true, message: "Please input title of table!" }]
          })(
            <Input
              prefix={
                <Icon
                  type="double-right"
                  style={{ color: "rgba(0,0,0,.25)" }}
                />
              }
              placeholder="Table title"
              name="title"
            />
          )}
        </Form.Item>
        <Form.Item
          label="Priority"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 12 }}
        >
          {getFieldDecorator("priority", {
            rules: [{ required: true, message: "Please choose priority!" }]
          })(<InputNumber min={1} max={100} placeholder="1" name="priority" />)}
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const AddTable = Form.create({ name: "add_table" })(AddTableI);
export default AddTable;
