import React, { Component } from "react";
import styled from "styled-components";
import { Draggable } from "react-beautiful-dnd";
import { Button } from 'antd';
import Cookies from 'js-cookie';

const Container = styled.div`
  padding: 8px;
  margin-bottom: 8px;
  border-radius: 5px;
  background: #001529;
  color: #fff;
`;

class Task extends Component {
  state = {};

  removeTask = (taskId, tabId) => {
    const values = {
      session: Cookies.get("sessionData"),
      taskId,
      tabId
    }

    fetch("https://apitproject.promeger.com/tables/removeTask", {
      method: "POST",
      body: JSON.stringify(values),
      crossOrigin: true,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        window.location.reload();
      });
  };

  render() {
    return (
      <Draggable draggableId={this.props.task.ids} index={this.props.index}>
        {provided => (
          <Container
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
          >
            {this.props.task.desc}
            {this.props.task.desc !== 'No tasks' ? <Button
              type="danger"
              shape="circle"
              icon="close"
              size="small"
              style={{float: 'right'}}
              onClick={() => this.removeTask(this.props.task.taskId, this.props.tabId)}
            /> :
            ""
        }
          </Container>
        )}
      </Draggable>
    );
  }
}

export default Task;
